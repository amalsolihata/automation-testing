package com.icehousecorp.android.test.functional;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import net.thucydides.core.webdriver.DriverSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AndroidDriverSource implements DriverSource {

    public static AndroidDriver<AndroidElement> ANDROID_DRIVER;

    private DesiredCapabilities dc;

    @Override
    public WebDriver newDriver() {
        dc = new DesiredCapabilities();
        dc.setCapability("automationName", "uiautomator2");
        dc.setCapability("deviceName", "Android");
        dc.setCapability("platformName", "Android");
        dc.setCapability("app", "");
        dc.setCapability("appPackage", "com.example.android.architecture.blueprints.todomvvmlive.mock");
        dc.setCapability("appActivity", "com.example.android.architecture.blueprints.todoapp.tasks.TasksActivity");
        dc.setCapability("noReset", true);
//        dc.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);

        try {
            ANDROID_DRIVER = new AndroidDriver<AndroidElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return ANDROID_DRIVER;
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }
}
