package com.icehousecorp.android.test.functional.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataObject<T> {

    @SerializedName("data")
    @Expose
    private T data;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
